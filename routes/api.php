<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Trash
Route::get('/trash', 'TrashController@index');
Route::post('/trash/store', 'TrashController@store');
Route::get('/trash/edit/{id}', 'TrashController@getArticle');
Route::get('/trash/show/{id}', 'TrashController@getArticle');
Route::put('/trash/update/{id}', 'TrashController@update');
Route::delete('/trash/delete/{id}', 'TrashController@delete');

// Trash Type
Route::get('/trash_types', 'TrashtypesController@index');
Route::post('/trash_types/store', 'TrashtypesController@store');
Route::get('/trash_types/edit/{id}', 'TrashtypesController@getArticle');
Route::get('/trash_types/show/{id}', 'TrashtypesController@getArticle');
Route::put('/trash_types/update/{id}', 'TrashtypesController@update');
Route::delete('/trash_types/delete/{id}', 'TrashtypesController@delete');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
