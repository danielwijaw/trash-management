<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trash_type extends Model
{
    protected $table = "trash_types";
    protected $fillable = ['type'];
}
