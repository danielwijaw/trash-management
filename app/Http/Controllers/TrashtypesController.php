<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TrashtypesController extends Controller
{
    public function index(Request $request)
    {
        if($request->get('format') != null){
            if($request->get('format')=='select'){
                $trashTypes = \App\Trash_type::get();
            }
        }else{
            $trashTypes = \App\Trash_type::paginate(10);
        }
        return $trashTypes;
    }
 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'type' => 'required'
        ]);
 
        $project = \App\Trash_type::create([
          'type' => $validatedData['type']
        ]);
 
        $msg = [
            'success' => true,
            'message' => 'Trash Types created successfully!'
        ];
 
        return response()->json($msg);
    }
 
    public function getArticle($id) // for edit and show
    {
        $trashTypes = \App\Trash_type::find($id);
 
        return $trashTypes;
    }
 
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
          'type' => 'required'
        ]);
 
        $trashTypes = \App\Trash_type::find($id);
        $trashTypes->type = $validatedData['type'];
        $trashTypes->save();
 
        $msg = [
            'success' => true,
            'message' => 'Trash Types updated successfully'
        ];
 
        return response()->json($msg);
    }
 
    public function delete($id)
    {
        $trashTypes = \App\Trash_type::find($id);
        if(!empty($trashTypes)){
            $trashTypes->delete();
            $msg = [
                'success' => true,
                'message' => 'Trash Types deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Trash Types deleted failed!'
            ];
            return response()->json($msg);
        }
    }
}
