<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrashController extends Controller
{
    public function index()
    {
        $trash = DB::table('trashes')->selectRaw('trashes.id, trashes.trash, trashes.types_id, type')->leftJoin('trash_types', 'trash_types.id', '=', 'trashes.types_id')->paginate(10);
        return $trash;
    }
 
    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'trash' => 'required',
          'types_id' => 'required'
        ]);
 
        $project = \App\Trash::create([
          'trash' => $validatedData['trash'],
          'types_id' => $validatedData['types_id']
        ]);
 
        $msg = [
            'success' => true,
            'message' => 'Trash created successfully!'
        ];
 
        return response()->json($msg);
    }
 
    public function getArticle($id) // for edit and show
    {
        $trash = DB::table('trashes')->selectRaw('trashes.id, trashes.trash, trashes.types_id, type')->leftJoin('trash_types', 'trash_types.id', '=', 'trashes.types_id')->where('trashes.id', $id)->first();
        return response()->json($trash);
    }
 
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
          'trash' => 'required',
          'types_id' => 'required'
        ]);
 
        $trash = \App\Trash::find($id);
        $trash->trash = $validatedData['trash'];
        $trash->types_id = $validatedData['types_id'];
        $trash->save();
 
        $msg = [
            'success' => true,
            'message' => 'Trash updated successfully'
        ];
 
        return response()->json($msg);
    }
 
    public function delete($id)
    {
        $trash = \App\Trash::find($id);
        if(!empty($trash)){
            $trash->delete();
            $msg = [
                'success' => true,
                'message' => 'Trash deleted successfully!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Trash deleted failed!'
            ];
            return response()->json($msg);
        }
    }
}
