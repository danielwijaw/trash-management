require('./bootstrap');
 
window.Vue = require('vue');
 
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import App from './components/App.vue';
import VueSweetalert2 from 'vue-sweetalert2'; 
import 'sweetalert2/dist/sweetalert2.min.css';
import VueProgressBar from 'vue-progressbar';
 
Vue.use(VueRouter);
Vue.use(VueSweetalert2);
Vue.use(VueAxios, axios);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '2px'
})
 
const TrashIndex = resolve => require(['./components/TrashIndex.vue'], resolve);
const TrashCreate = resolve => require(['./components/TrashCreate.vue'], resolve);
const TrashShow = resolve => require(['./components/TrashShow.vue'], resolve);
const TrashEdit = resolve => require(['./components/TrashEdit.vue'], resolve);
const TrashtypesIndex = resolve => require(['./components/TrashtypesIndex.vue'], resolve);
const TrashtypesCreate = resolve => require(['./components/TrashtypesCreate.vue'], resolve);
const TrashtypesShow = resolve => require(['./components/TrashtypesShow.vue'], resolve);
const TrashtypesEdit = resolve => require(['./components/TrashtypesEdit.vue'], resolve);
const PageNotFound = resolve => require(['./components/PageNotFound.vue'], resolve);    
 
const routes = [
    {
        name: 'Trash',
        path: '/',
        component: TrashIndex
    },
    {
        name: 'TrashCreate',
        path: '/trash/create',
        component: TrashCreate
    },
    {
        name: 'TrashShow',
        path: '/trash/show/:id',
        component: TrashShow
    },
    {
        name: 'TrashEdit',
        path: '/trash/edit/:id',
        component: TrashEdit
    },
    {
        name: 'TrashtypeHome',
        path: '/trash_type',
        component: TrashtypesIndex
    },
    {
        name: 'TrashtypeCreate',
        path: '/trash_type/create',
        component: TrashtypesCreate
    },
    {
        name: 'TrashtypeEdit',
        path: '/trash_type/edit/:id',
        component: TrashtypesEdit
    },
    {
        name: 'TrashtypeShow',
        path: '/trash_type/show/:id',
        component: TrashtypesShow
    },
    {
        name: '404',
        path: '*',
        component: PageNotFound
    }
];
 
const router = new VueRouter({ mode: 'history', routes: routes});
const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');